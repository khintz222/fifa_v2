import Controller.CSVFileReader
import Controller.PlayerMapper
import model.Player

fun main() {

    val lines = CSVFileReader("C:\\Users\\krzysztofhi\\Downloads\\Zeszyt1_v2\\Zeszyt1.csv").read()

    lines.map{ PlayerMapper}

    var list = arrayListOf<Player>()
    lines.forEach {
        list.add(PlayerMapper.map(it))
    }

    list.forEach {
        println(it)
    }
}